var gulp = require('gulp'),
	sass = require('gulp-sass'),
	autoprefixer = require('gulp-autoprefixer'),
	concat = require('gulp-concat'),
	uglify = require('gulp-uglify'),
	addSrc = require('gulp-add-src'),
	connect = require('gulp-connect'),
	open = require('open'),
	inject = require('gulp-inject'),
	rename = require('gulp-rename'),
	templateCache = require('gulp-angular-templatecache');

var paths = {
	libs: [
		'bower_components/jquery/dist/jquery.js',
		'bower_components/angular/angular.js',
		'bower_components/angular-mocks/angular-mocks.js',
		'bower_components/angular-ui-router/release/angular-ui-router.js',
		'bower_components/angular-bootstrap/ui-bootstrap-tpls.js',
		'bower_components/lodash/lodash.js',
		'bower_components/jshashes/hashes.js'
	],
	styles: {
		base: 'sass/',
		files: '**/*.scss',
		libs: ['bower_components/bootstrap/dist/css/bootstrap.css']
	},
	scripts: ['libs/*.js', 'js/**/*.js'],
	templates: 'js/**/*.html'
};

gulp.task('libs', function() {
	gulp
		.src(paths.libs)
		.pipe(concat('libs.js'))
		.pipe(uglify())
		.pipe(gulp.dest('libs'));
});

gulp.task('styles', function() {
	gulp
		.src(paths.styles.base + 'style.scss')
		.pipe(sass())
		.pipe(autoprefixer({
			browsers: ['last 2 versions']
		}))
		.pipe(addSrc(paths.styles.libs))
		.pipe(concat('style.css'))
		.pipe(gulp.dest('css'));
});

gulp.task('templates', function() {
	gulp.src(paths.templates)
		.pipe(templateCache({
			module: 'SWD.templates',
			standalone: true
		}))
		.pipe(gulp.dest('js'));
});

gulp.task('index', function() {
	gulp.src('index.tpl.html')
		.pipe(inject(gulp.src(paths.scripts, {
			read: false
		})))
		.pipe(rename('index.html'))
		.pipe(gulp.dest(''));
});

gulp.task('assets', function() {
	gulp
		.src(['bower_components/bootstrap/dist/fonts/**/*'])
		.pipe(gulp.dest('fonts'));
});

gulp.task('connect', function() {
	connect.server({
		root: '',
		port: '7000',
		livereload: true,
		middleware: function(connect, options) {
			return [
				function(req, res, next) {
					if (req.url.search(/^\/css|fonts|js/) === -1) {
						req.url = '/index.html';
					}

					return next();
				}/*, // static files
				 connect.static(options.base[0])*/
			];
		}
	});
});

gulp.task('connect.reload', function() {
	gulp
		.src('*.html')
		.pipe(connect.reload());
});

gulp.task('open', function() {
	open('http://localhost:7000');
});

gulp.task('watch', function() {
	gulp.watch(paths.styles.base + paths.styles.files, ['styles']);
	gulp.watch(paths.templates, ['templates']);
	gulp.watch(['css/**/*.css', '*.html', 'js/**/*.js'], ['connect.reload']);
	gulp.watch('index.tpl.html', ['index']);
	gulp.watch(paths.scripts, function(event) {
		event.type === 'added' || event.type === 'deleted' && gulp.run('index');
	});
});

gulp.task('build', ['styles', 'libs', 'assets', 'templates', 'index']);
gulp.task('launch', ['build', 'connect', 'open']);
gulp.task('default', ['build', 'connect', 'watch']);
