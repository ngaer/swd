'use strict';

angular.module('SWD.mocks.items', ['SWD.mocks.base'])

.run(function(ApiMockBase) {
	var items = [{
			id: '18143199',
			name: 'Sterling Silver Elephant Necklace and Earrings Jewelry Set',
			color: 'blue',
			date: '2015-06-29T02:13:14',
			price: 8.4,
			rating: 3.5,
			inStock: true,
			image: 'https://images-na.ssl-images-amazon.com/images/I/51kc9r9zg0L.jpg'
		}, {
			id: '96865323',
			name: 'Fodor\'s Paris 2015',
			color: 'yellow',
			date: '2015-06-21T09:51:20',
			price: 71.2,
			rating: 0.9,
			inStock: false,
			image: 'https://images-na.ssl-images-amazon.com/images/I/51dpCZGHkXL.jpg'
		}, {
			id: '0827f3ab',
			name: 'D&Y Women\'s Tropical Print Loop',
			color: 'green',
			date: '2015-06-24T03:23:42',
			price: 101.9,
			rating: 0.9,
			inStock: true,
			image: 'https://images-na.ssl-images-amazon.com/images/I/51yO-Dn-nUL.jpg'
		}, {
			id: '0ae74d25',
			name: 'Bull Frog Surf Formula Gel Sunscreen SPF 36',
			color: 'yellow',
			date: '2015-07-03T03:13:32',
			price: 193.8,
			rating: 1.3,
			inStock: true,
			image: 'http://ecx.images-amazon.com/images/I/716aHCZQ8PL._SY679_.jpg'
		}, {
			id: '2960c382',
			name: 'Tronsmart Quick charger 2.0 car charger',
			color: 'red',
			date: '2015-06-28T05:14:16',
			price: 41.1,
			rating: 5.0,
			inStock: false,
			image: 'https://images-na.ssl-images-amazon.com/images/I/41PHut2QEAL.jpg'
		}, {
			id: '4dddd360',
			name: 'Fodor\'s Ireland 2015',
			color: 'green',
			date: '2015-07-09T07:09:23',
			price: 124.2,
			rating: 1.8,
			inStock: false,
			image: 'https://images-na.ssl-images-amazon.com/images/I/51bWp6jvHyL.jpg'
		}, {
			id: '51c4a7ef',
			name: 'Fodor\'s Spain 2015',
			color: 'white',
			date: '2015-07-10T10:02:31',
			price: 192.1,
			rating: 2.6,
			inStock: true,
			image: 'https://images-na.ssl-images-amazon.com/images/I/51J8Rpk%2BRyL.jpg'
		}, {
			id: '5a69d8b0',
			name: 'Minisuit Mini Grip Car Vent Mount',
			color: 'green',
			date: '2015-06-29T09:49:32',
			price: 45.2,
			rating: 3.9,
			inStock: true,
			image: 'http://ecx.images-amazon.com/images/I/71CPpaRPnNL._SX522_.jpg'
		}, {
			id: '5baa1d9c',
			name: 'Fodor\'s Hawaii 2015',
			color: 'green',
			date: '2015-07-04T12:18:30',
			price: 196.8,
			rating: 1.8,
			inStock: true,
			image: 'https://images-na.ssl-images-amazon.com/images/I/51whPyAw3EL.jpg'
		}, {
			id: '62c8ba72',
			name: 'Extra 50% off on Baggallini Waltz Carry-On Softside Rolling Tote with 2 Wheels',
			color: 'yellow',
			date: '2015-06-21T09:50:10',
			price: 78.9,
			rating: 3.1,
			inStock: true,
			image: 'https://images-na.ssl-images-amazon.com/images/I/31C5BPdiNIL.jpg'
		}, {
			id: '6a9e4660',
			name: 'Sterling Silver Love You To The Moon Bead Charm Bracelet',
			color: 'yellow',
			date: '2015-07-06T06:26:57',
			price: 95.9,
			rating: 2.7,
			inStock: true,
			image: 'https://images-na.ssl-images-amazon.com/images/I/4172lhQ3GNL.jpg'
		}, {
			id: '7e5eede2',
			name: 'Rudiger Women\'s Gold-Tone Diamond-Accented Watch',
			color: 'white',
			date: '2015-06-23T01:03:38',
			price: 126.9,
			rating: 3.5,
			inStock: false,
			image: 'https://images-na.ssl-images-amazon.com/images/I/419PCEHwHVL.jpg'
		}, {
			id: '9b41c1e1',
			name: 'Fodor\'s Arizona & the Grand Canyon 2015',
			color: 'black',
			date: '2015-07-05T11:59:54',
			price: 102.3,
			rating: 1.5,
			inStock: false,
			image: 'https://images-na.ssl-images-amazon.com/images/I/51pRD4ibMnL.jpg'
		}, {
			id: 'd1178048',
			name: 'Fodor\'s The Complete Guide to the National Parks of the West',
			color: 'black',
			date: '2015-07-08T05:36:09',
			price: 78.2,
			rating: 4.5,
			inStock: false,
			image: 'https://images-na.ssl-images-amazon.com/images/I/51TTSArkpoL.jpg'
		}, {
			id: 'dd8d92b1',
			name: 'Fodor\'s Maui 2015',
			color: 'red',
			date: '2015-07-01T01:43:45',
			price: 83.7,
			rating: 1.2,
			inStock: false,
			image: 'https://images-na.ssl-images-amazon.com/images/I/518Ze0uW%2BBL.jpg'
		}, {
			id: 'f1cf9735',
			name: 'Galaxy S6 Edge Case, [Scratch Resistant] i-Blason **Clear** [Halo Series] Samsung Galaxy S6 Edge ',
			color: 'green',
			date: '2015-06-22T03:34:57',
			price: 182.6,
			rating: 3.1,
			inStock: false,
			image: 'http://ecx.images-amazon.com/images/I/71cd6nu4-LL._SX522_.jpg'
		}];

	ApiMockBase.get('/api/items', function(method, requestUrl, data) {
		var responseItems = items;

		if (data) {
			_.keys(data).forEach(function(key) {
				var value = data[key];

				switch (key) {
					case 'dateFrom':
						responseItems = responseItems.filter(function(item) {
							return item.date >= value;
						});
						break;
					case 'dateTo':
						responseItems = responseItems.filter(function(item) {
							return item.date <= value;
						});
						break;
					case 'inStockOnly':
						responseItems = responseItems.filter(function(item) {
							return item.inStock === true;
						});
						break;
					case 'priceFrom':
						responseItems = responseItems.filter(function(item) {
							return item.price >= value;
						});
						break;
					case 'priceTo':
						responseItems = responseItems.filter(function(item) {
							return item.price <= value;
						});
						break;
					case 'color':
						responseItems = responseItems.filter(function(item) {
							return item.color === value;
						});
						break;
					case 'ids':
						responseItems = responseItems.filter(function(item) {
							return value.indexOf(item.id) !== -1;
						});
						break;
				}
			});
		}

		return [200, responseItems];
	});
});
