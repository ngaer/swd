'use strict';

angular.module('SWD.mocks.auth', ['SWD.mocks.base'])

.run(function(ApiMockBase) {
	var MD5 = new Hashes.MD5,
		users = (function(users) {
			var data = [];

			if (users) {
				try {
					data = JSON.parse(users);
				} catch(e) {}
			}

			return data;
		})(localStorage.getItem('users'));

	ApiMockBase.post('/api/login', function(method, requestUrl, data) {
		var user = _.find(users, { email: data.email });

		if (user && MD5.hex(data.password) === user.password) {
			return [200, user];
		} else {
			return [401, {
				error: 'Authentication fail'
			}];
		}
	});

	ApiMockBase.post('/api/signup', function(method, requestUrl, data) {
		var user = _.find(users, { email: data.email });

		if (user) {
			return [400, {
				error: 'Email is already taken'
			}];
		} else {
			data.id = users.length + 1;
			data.password = MD5.hex(data.password);

			users.push(data);
			localStorage.setItem('users', JSON.stringify(users));

			return [200, data];
		}
	});
});
