'use strict';

angular.module('SWD.mocks.base', ['ngMockE2E'])

.factory('ApiMockBase', function($httpBackend) {
	var	mockBase = function(method, url, response, status) {
			var requestHandler = $httpBackend.when(method, url);

			if (typeof response === 'function') {
				return requestHandler.respond(function(method, requestUrl, data) {
					if (typeof data === 'string') {
						try {
							data = JSON.parse(data);
						} catch (e) {}
					}

					if (requestUrl.indexOf('?') !== -1) {
						// Getting request params from query string for GET requests
						var params = requestUrl.slice(requestUrl.indexOf('?') + 1).split('&');

						data = data || {};

						_.forEach(params, function(param) {
							var paramArray = param.split('='),
								key = paramArray[0],
								value = paramArray[1];

							if (key.indexOf('%5B%5D') !== -1) {
								key = key.replace('%5B%5D', '');

								if (data[key]) {
									data[key].push(value);
								} else {
									data[key] = [value];
								}
							} else {
								data[key] = value;
							}
						});
					}

					return response(method, requestUrl, data);
				});
			} else {
				return requestHandler.respond(status || 200, response);
			}
		},
		ApiMockBase = {};

	_.forEach(['get', 'post', 'put', 'delete'], function(method) {
		ApiMockBase[method] = function(url, response, status) {
			return mockBase(method.toUpperCase(), url, response, status);
		};
	});

	return ApiMockBase;
});
