'use strict';

angular.module('SWD.controller.card', [])

.controller('CartCtrl', function($scope, $rootScope, items, $modalInstance) {
	var vm = this;

	vm.items = items;
	vm.close = close;

	$scope.$watch(function() {
		return $rootScope.currentUser.cart;
	}, function(value, oldValue) {
		if (value !== oldValue) {
			vm.items = vm.items.filter(function(item) {
				return value.indexOf(item.id) !== -1;
			});
		}
	});

	function close() {
		$modalInstance.dismiss('cancel');
	}
});
