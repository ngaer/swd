'use strict';

angular.module('SWD.controller.search', [
	'ui.router',
	'SWD.service.items',
	'SWD.directive.item'
])

.config(function($stateProvider) {
	$stateProvider.state('items', {
		url: '/items',
		controller: 'SearchCtrl',
		controllerAs: 'search',
		templateUrl: 'components/items/search/search.html',
		requireAuth: true,
		resolve: {
			items: function(Items) {
				return Items.search();
			}
		}
	});
})

.controller('SearchCtrl', function($scope, $filter, items, Items) {
	var vm = this;

	vm.data = items;
	vm.filters = {
		dateFrom: null,
		dateTo: null,
		inStockOnly: false,
		priceFrom: null,
		priceTo: null,
		color: ''
	};

	$scope.$watch(function() {
		return vm.filters;
	}, function(value, oldValue) {
		if (value !== oldValue) {
			var filters = {};

			_.keys(vm.filters).forEach(function(key) {
				var value = vm.filters[key];

				if (value) {
					if (key === 'dateFrom') {
						value = $filter('date')(value, 'yyyy-MM-ddT00:00:00');
					} else if (key === 'dateTo') {
						value = $filter('date')(value, 'yyyy-MM-ddT23:59:59');
					}

					filters[key] = value;
				}
			});

			Items.search(filters).then(function(data) {
				vm.data = data;
			});
		}
	}, true);
});
