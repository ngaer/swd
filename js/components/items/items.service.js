'use strict';

angular.module('SWD.service.items', [])

.factory('Items', function($q, $http) {
	return {
		get: function(ids) {
			return $http
				.get('/api/items', {
					data: {
						ids: _.isArray(ids) ? ids : [ids]
					}
				})
				.then(function(response) {
					return response.data;
				});
		},
		search: function(filters) {
			return $http
				.get('/api/items', {
					data: filters
				})
				.then(function(response) {
					return response.data;
				});
		}
	}
});
