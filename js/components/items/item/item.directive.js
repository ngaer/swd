'use strict';

angular.module('SWD.directive.item', [
	'SWD.service.user',
	'SWD.directive.rating'
])

.controller('ItemCtrl', function($scope, User) {
	var vm = this;

	vm.addToCart = User.addToCart;
	vm.removeFromCart = User.removeFromCart;
})

.directive('swdItem', function() {
	return {
		restrict: 'E',
		scope: {
			data: '=item'
		},
		replace: true,
		bindToController: true,
		templateUrl: 'components/items/item/item.html',
		controller: 'ItemCtrl',
		controllerAs: 'item'
	}
});
