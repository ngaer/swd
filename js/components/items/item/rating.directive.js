'use strict';

angular.module('SWD.directive.rating', [])

.directive('swdRating', function() {
	return {
		restrict: 'E',
		scope: {
			value: '='
		},
		replace: true,
		templateUrl: 'components/items/item/rating.html'
	}
})
