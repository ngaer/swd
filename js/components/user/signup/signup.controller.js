'use strict';

angular.module('SWD.controller.signup', [
	'ui.router',
	'SWD.service.user'
])

.config(function($stateProvider) {
	$stateProvider.state('signup', {
		url: '/signup',
		controller: 'SignUpCtrl',
		controllerAs: 'signUp',
		templateUrl: 'components/user/signup/signup.html'
	});
})

.controller('SignUpCtrl', function($state, User, Alert) {
	var vm = this;

	vm.data = {
		email: '',
		password: '',
		passwordConfirm: ''
	};
	vm.submit = submit;

	function submit() {
		if (vm.form.$valid) {
			User
				.signUp(vm.data.email, vm.data.password)
				.success(function() {
					$state.go('items');
				})
				.error(function(data) {
					data.error && Alert.add(data.error);
				});
		}
	}
});
