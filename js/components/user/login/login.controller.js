'use strict';

angular.module('SWD.controller.login', [
	'ui.router',
	'SWD.service.user'
])

.config(function($stateProvider) {
	$stateProvider.state('login', {
		url: '/login',
		controller: 'LoginCtrl',
		controllerAs: 'login',
		templateUrl: 'components/user/login/login.html'
	});
})

.controller('LoginCtrl', function($state, User, Alert) {
	var vm = this;

	vm.data = {
		email: '',
		password: ''
	};
	vm.submit = submit;

	function submit() {
		if (vm.form.$valid) {
			User
				.login(vm.data.email, vm.data.password)
				.success(function() {
					$state.go('items');
				})
				.error(function(data) {
					data.error && Alert.add(data.error);
				});
		}
	}
});
