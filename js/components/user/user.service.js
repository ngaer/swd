'use strict';

angular.module('SWD.service.user', [])

.factory('User', function($q, $http, $rootScope) {
	var User = {};

	User.data = null;

	User.setData =  function(data) {
		User.data = data;
		$rootScope.currentUser = data;
		sessionStorage.setItem('user', JSON.stringify(User.data));
	};

	User.isAuthorized = function() {
		return User.data && User.data.id;
	};

	User.login = function(email, password) {
		return $http.post('/api/login', {
			email: email,
			password: password
		}).success(function(data) {
			User.setData(data);
		});
	};

	User.signUp = function(email, password) {
		return $http.post('/api/signup', {
			email: email,
			password: password
		}).success(function(data) {
			User.setData(data);
		});
	};

	User.addToCart = function(id) {
		User.data.cart = User.data.cart || [];
		User.data.cart.push(id);
		User.setData(User.data);
	};

	User.removeFromCart = function(id) {
		User.data.cart = (User.data.cart || []).filter(function(itemId) {
			return itemId !== id;
		});
		User.setData(User.data);
	};

	var user = sessionStorage.getItem('user');

	if (user) {
		try {
			user = JSON.parse(user);
		} catch (e) {}

		if (typeof user === 'object') {
			User.setData(user);
		}
	}

	return User;
});
