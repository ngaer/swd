'use strict';

angular.module('SWD.service.common', [])

.factory('Alert', function($rootScope, $timeout) {
	var Alert = {};

	$rootScope.alerts = [];

	Alert.add = function(message, type) {
		var id = new Date().getTime();

		$rootScope.alerts.push({
			id: id,
			message: message,
			type: type || 'danger'
		});

		$timeout(function() {
			Alert.remove(id);
		}, 5000);
	};

	$rootScope.removeAlert = Alert.remove = function(id) {
		$rootScope.alerts = $rootScope.alerts.filter(function(alert) {
			return alert.id !== id;
		});
	};

	return Alert;
});
