'use strict';

angular.module('SWD.directive.common', [])

.directive('ngEquals', function() {
	return {
		restrict: 'A',
		require: 'ngModel',
		scope: {
			original: '=ngEquals'
		},
		link: function(scope, elm, attrs, ngModel) {
			scope.$watch(function () {
				return [scope.original, ngModel.$viewValue];
			}, function (values) {
				ngModel.$setValidity('equals', values[0] === values[1]);
			}, true);
		}
	};
});
