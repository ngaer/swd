angular.module('SWD', [
	'ui.router',
	'ui.bootstrap',
	'SWD.templates',
	'SWD.mocks.auth',
	'SWD.mocks.items',
	'SWD.directive.common',
	'SWD.service.common',
	'SWD.controller.login',
	'SWD.controller.signup',
	'SWD.controller.search',
	'SWD.controller.card'
])

.config(function($locationProvider, $urlRouterProvider, $stateProvider) {
	$locationProvider.html5Mode({
		enabled: true,
		requireBase: false
	});

	$urlRouterProvider.otherwise('/items');
})

.run(function($httpBackend, $rootScope, $state, User) {
	($httpBackend.whenGET(/(.*)\.html/).passThrough || angular.noop)();

	$rootScope.$on('$stateChangeStart', function (event, nextState) {
		if (nextState.requireAuth && !User.isAuthorized()) {
			event.preventDefault();
			$state.go('login');
		}
	});
})

.controller('RootCtrl', function($state, $modal, User) {
	var vm = this;

	vm.logout = logout;
	vm.showCart = showCart;

	function logout() {
		User.setData(null);
		$state.go('login');
	}

	function showCart() {
		$modal.open({
			templateUrl: 'components/items/cart/cart.html',
			controller: 'CartCtrl',
			controllerAs: 'cart',
			size: 'lg',
			resolve: {
				items: function(User, Items) {
					return Items.get(User.data.cart);
				}
			}
		});
	}
});
